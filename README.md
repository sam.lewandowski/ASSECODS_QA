Zadanie zawiera projekt z testami SOAPUI i dokumentacje TP (testów penetracyjnych)
i TZ (testów zgodności z dokumentacją).
Starałem się skupić na sprawdzeniu działania API i pomimo sporych braków w dokumentacji
spróbować stworzyć testy w środowisku SOAPUI. Zabrakło mi w nich (prawdopodobnie) popprawnej obsługi
zewnętrznych skryptów, żeby przekazywać token dynamicznie do dalszych Test Case'ów 
(niestety na chwilę obecną wprowadzam token w headerze ręcznie) 
Starałem się wykazać moją świadomość pewnych błędów w API (np. nieogarniczonych na długość haseł)
jednocześnie nie tworząc kilkudziesięciu manualnie tworzonych test case'ów z jednego szablonu w edytorze tekstowym.
W folderze TP, wykazałem pewne wrażliwości systemu (pomijając oczywistości tj. połączenie nieszyfrowane), próbowałem również znaleźć takie wrażliwości w dokumentacji OWASP i zaimplementować je,
jednak bezskutecznie, wszelkie automatyczne skany XSS i SQLInjection np. Acunetix wymagały podania dowodu posiadania praw do domeny.

Samuel Lewandowski 25.06.2017